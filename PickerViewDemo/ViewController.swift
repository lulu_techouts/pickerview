//
//  ViewController.swift
//  PickerViewDemo
//
//  Created by Dhanvanthi P on 31/05/18.
//  Copyright © 2018 Dhanvanthi P. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore

class ViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var pickerTextField: UITextField!
    
    @IBOutlet weak var numberPickerTextField: UITextField!
    
    var pickOption = ["one", "two", "three", "seven", "fifteen"]
    var pickOption1 = ["Dhanvanthi", "Pinky", "Hema", "Tulasi", "Yogesh"]
    
    var picker = UIPickerView()
    var picker1 = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        picker.dataSource = self
        
        picker1.delegate = self
        picker1.dataSource = self
        
        pickerTextField.inputView = picker
        numberPickerTextField.inputView = picker1
        // Do any additional setup after loading the view, typically from a nib.
        
        let loginButton = LoginButton(readPermissions: [.publicProfile, .email, .userFriends])
        loginButton.center = view.center
        view.addSubview(loginButton)
        
        if let accessToken = AccessToken.current {
            print("user is logged in with access token: \(accessToken)")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerTextField.tag == 0 {
             return pickOption.count
        } else {
            return pickOption1.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerTextField.tag == 0 {
            pickerTextField.text = pickOption[row]
        } else {
            numberPickerTextField.text = pickOption1[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerTextField.tag == 0 {
             return pickOption[row]
        } else  {
            return pickOption1[row]
        }
    }
}

